const path = require('path')
const PATH_SRC = path.resolve(__dirname, 'src')
const PATH = dir => path.resolve(__dirname, 'src', ...dir)

module.exports = function override(config, env) {
  config.entry = {
    main: './src/index.jsx'
  }
  config.resolve = {
    alias: {
      src: PATH_SRC,
      assets: PATH(['assets']),
      components: PATH(['components']),
      constants: PATH(['constants']),
      helpers: PATH(['helpers']),
      hooks: PATH(['hooks']),
      language: PATH(['language']),
      layouts: PATH(['layouts']),
      pages: PATH(['pages']),
      public: PATH(['public']),
      router: PATH(['router']),
      service: PATH(['service']),
      store: PATH(['store']),
      scss: PATH(['scss'])
    },
    enforceExtension: false,
    extensions: ['.jsx', '.js', '.json'],
    mainFiles: ['index']
  }

  return config
}
