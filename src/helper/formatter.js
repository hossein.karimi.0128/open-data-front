export const toEnglish = input => {
  const map = {
    // arabic
    '٠': '0',
    '١': '1',
    '٢': '2',
    '٣': '3',
    '٤': '4',
    '٥': '5',
    '٦': '6',
    '٧': '7',
    '٨': '8',
    '٩': '9',
    // persian
    '۰': '0',
    '۱': '1',
    '۲': '2',
    '۳': '3',
    '۴': '4',
    '۵': '5',
    '۶': '6',
    '۷': '7',
    '۸': '8',
    '۹': '9'
  }
  const numbers = input.split('')
  numbers.forEach((number, index) => {
    if (Object.prototype.hasOwnProperty.call(map, numbers[index])) {
      numbers[index] = map[number]
    }
  })
  return numbers.join('')
}

export const toPersian = input => {
  const map = {
    0: '۰',
    1: '۱',
    2: '۲',
    3: '۳',
    4: '۴',
    5: '۵',
    6: '۶',
    7: '۷',
    8: '۸',
    9: '۹'
  }
  const numbers = input.split('')
  numbers.forEach((number, index) => {
    if (Object.prototype.hasOwnProperty.call(map, numbers[index])) {
      numbers[index] = map[number]
    }
  })

  return numbers.join('')
}
