import { Col, Container, Form, Row, Spinner } from '@themesberg/react-bootstrap'
import React, { useState } from 'react'
import { Cell, Column, HeaderCell, Table } from 'rsuite-table'
import 'rsuite-table/dist/css/rsuite-table.css'

export default props => {
  const { items, columns, loading = false, actions, children } = props
  const [isLoading, setIsLoading] = useState(loading)
  const [searched, setSearched] = useState(items)

  const onChangeHandle = event => {
    setIsLoading(true)
    const search = event.target.value
    setSearched(items.filter(item => item.name.includes(search)))
    setTimeout(() => {
      setIsLoading(false)
    }, 500)
  }

  return (
    <Container fluid className={'p-0 mt-4'}>
      <Row className={'mb-3'}>
        <Col xs={{ span: 3 }}>
          <Form.Control
            type={'search'}
            placeholder={'جستجو...'}
            onChange={onChangeHandle}
          />
        </Col>
        <Col>{actions}</Col>
      </Row>
      <Row>
        <Col>
          <Table
            data={searched}
            rtl
            bordered
            hover
            cellBordered
            autoHeight
            renderLoading={() => (
              <div
                className={
                  'position-absolute top-0 start-0 bottom-0 end-0 bg-white'
                }
              >
                <div className={'rs-table-body-info'}>
                  <Spinner animation={'border'} variant={'primary'} />
                </div>
              </div>
            )}
            renderEmpty={() => (
              <div
                className={
                  'position-absolute top-0 start-0 bottom-0 end-0 bg-white'
                }
              >
                <div className={'rs-table-body-info fs-6 text-muted'}>
                  داده‌ای یافت نشد
                </div>
              </div>
            )}
            headerHeight={48}
            rowHeight={42}
            className={'fs-6'}
            loading={isLoading}
            affixHorizontalScrollbar
          >
            {columns
              ? columns.map(({ key, value, ...rest }, index) => (
                  <Column {...rest} key={`col-${key}-${index}`}>
                    <HeaderCell>{value}</HeaderCell>
                    <Cell dataKey={key} />
                  </Column>
                ))
              : children}
          </Table>
        </Col>
      </Row>
    </Container>
  )
}
