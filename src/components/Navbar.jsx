import { faSignOutAlt, faUserCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  Container,
  Image,
  Nav,
  Navbar,
  NavDropdown
} from '@themesberg/react-bootstrap'
import Profile3 from 'assets/img/team/profile-picture-3.jpg'
import React from 'react'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { resetUser } from 'store/slices/UserSlice'

export default props => {
  const { title } = props
  const dispatch = useDispatch()
  const { push } = useHistory()

  const onLogOutUser = () => {
    dispatch(resetUser())
  }

  const onEditProfile = () => {
    push('/request/')
  }

  const onRenderUser = (
    <div className={'media py-0 d-flex align-items-center gap-2'}>
      <Image
        src={Profile3}
        className={'user-avatar md-avatar rounded-circle'}
      />
      <div className={'media-body text-truncate'}>علی اصغر مدیرروستا</div>
    </div>
  )

  return (
    <Navbar bg={'soft-primary'} className={'text-white py-1 user-select-none'}>
      <Container fluid>
        <h2 className={'fs-4 fw-bold m-0 fh-50 d-flex align-items-center'}>
          {title}
        </h2>
        <Nav className={'fh-50 d-flex align-items-center'}>
          <NavDropdown title={onRenderUser} as={Nav.Item} bsPrefix={'py-0'}>
            <NavDropdown.Item
              onClick={onEditProfile}
              className={''}
              as={Nav.Link}
            >
              <FontAwesomeIcon icon={faUserCircle} size={'lg'} pull={'right'} />
              حساب کاربری
            </NavDropdown.Item>

            <NavDropdown.Divider />
            <NavDropdown.Item
              onClick={onLogOutUser}
              className={'text-danger'}
              as={Nav.Link}
            >
              <FontAwesomeIcon icon={faSignOutAlt} size={'lg'} pull={'right'} />
              خروج از حساب کاربری
            </NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Container>
    </Navbar>
  )
}
