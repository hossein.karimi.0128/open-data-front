import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Badge, Image, Nav } from '@themesberg/react-bootstrap'
import Logo from 'assets/img/logo-dark.png'
import { Routes } from 'constants/routes'
import React, { useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { CSSTransition } from 'react-transition-group'
import SimpleBar from 'simplebar-react'

export default props => {
  const { isAdmin } = props
  const location = useLocation()
  const { pathname } = location
  const [show, setShow] = useState(false)
  const showClass = show ? 'show' : ''

  const NavItem = props => {
    const {
      title,
      link,
      external,
      target,
      icon,
      image,
      badgeText,
      badgeBg = 'secondary',
      badgeColor = 'primary'
    } = props

    const classNames = badgeText
      ? 'd-flex justify-content-start align-items-center justify-content-between'
      : ''
    const navItemClassName = link === pathname ? 'active' : ''
    const linkProps = external ? { href: link } : { as: Link, to: link }

    return (
      <Nav.Item className={navItemClassName} onClick={() => setShow(false)}>
        <Nav.Link {...linkProps} target={target} className={classNames}>
          <span>
            {icon && (
              <span className={'sidebar-icon'}>
                <FontAwesomeIcon icon={icon} />
              </span>
            )}
            {image && (
              <Image
                src={image}
                width={20}
                height={20}
                className={'sidebar-icon svg-icon'}
              />
            )}

            <span className={'sidebar-text'}>{title}</span>
          </span>
          {badgeText && (
            <Badge
              pill
              bg={badgeBg}
              text={badgeColor}
              className="badge-md notification-count ms-2"
            >
              {badgeText}
            </Badge>
          )}
        </Nav.Link>
      </Nav.Item>
    )
  }

  const onGenerateItems = () => {
    const items = Object.values(isAdmin ? Routes.Admin : Routes).filter(
      item => item.sidebar
    )

    return items.map(({ title, path, icon }, index) => (
      <NavItem
        key={`sidebar-item-${index}`}
        title={title}
        link={path}
        icon={icon}
      />
    ))
  }

  return (
    <CSSTransition timeout={300} in={show} classNames="sidebar-transition">
      <SimpleBar
        className={`collapse ${showClass} sidebar d-md-block bg-primary text-white`}
      >
        <div className="sidebar-inner px-2 py-4">
          <Nav className="flex-column pt-3 pt-md-0">
            <div className="d-flex align-items-center justify-content-center pb-4 gap-2">
              <Image src={Logo} className={'mb-2'} />
            </div>
            {/* {isAdmin ? (
              <>
                <NavItem
                  title={Routes.Admin.Dashboard.title}
                  link={Routes.Admin.Dashboard.path}
                  icon={faChartPie}
                />
                <NavItem
                  title={Routes.Admin.Request.title}
                  link={Routes.Admin.Request.path}
                  icon={faList}
                />
                <NavItem
                  title={Routes.Admin.Dataset.title}
                  link={Routes.Admin.Dataset.path}
                  icon={faDatabase}
                />
                <NavItem
                  title={Routes.Admin.User.title}
                  link={Routes.Admin.User.path}
                  icon={faUserFriends}
                />
                <NavItem
                  title={Routes.Admin.Role.title}
                  link={Routes.Admin.Role.path}
                  icon={faMagic}
                />
                <NavItem
                  title={Routes.Admin.Activity.title}
                  link={Routes.Admin.Activity.path}
                  icon={faChartLine}
                />
                <NavItem
                  title={Routes.Admin.Support.title}
                  link={Routes.Admin.Support.path}
                  icon={faHandsHelping}
                />
              </>
            ) : (
              <>
                <NavItem
                  title={Routes.Dashboard.title}
                  link={Routes.Dashboard.path}
                  icon={faChartPie}
                />
                <NavItem
                  title={Routes.Dataset.title}
                  link={Routes.Dataset.path}
                  icon={faDatabase}
                />
                <NavItem
                  title={Routes.Data.title}
                  link={Routes.Data.path}
                  icon={faFileAlt}
                />
                <NavItem
                  title={Routes.Request.title}
                  link={Routes.Request.path}
                  icon={faList}
                />
                <NavItem
                  title={Routes.Support.title}
                  link={Routes.Support.path}
                  icon={faHandsHelping}
                />
                <NavItem
                  title={Routes.Setting.title}
                  link={Routes.Setting.path}
                  icon={faCogs}
                />
              </>
            )} */}
            {onGenerateItems()}
          </Nav>
        </div>
      </SimpleBar>
    </CSSTransition>
  )
}
