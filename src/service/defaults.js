export const InitialConfig = {
  baseURL: `${process.env.REACT_APP_REQ_BASEURL}/${process.env.REACT_APP_REQ_APIPATH}`,
  timeout: parseInt(process.env.REACT_APP_REQ_TIMEOUT || '30'),
  headers: {
    Accept: 'application/json'
  }
}

export const InitialError = {
  response: {
    data: {
      status: 'fail',
      code: '500',
      message: 'در حال حاضر ارتباط با سرور برقرار نیست'
    }
  }
}

export const PayloadType = {
  default: 'application/json;charset=UTF-8',
  formData: 'multipart/form-data;charset=UTF-8'
}
