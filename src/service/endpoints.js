import axios from 'axios'
import { InitialConfig } from './defaults'

const Endpoints = {
  verifyMobile: {
    url: 'User/AuthWithMobileNumber',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  verifyOtp: {
    url: 'User/VerifyAuthWithMobileNumber',
    method: 'post',
    responseType: 'json',
    isFormData: false
  },
  fetchUser: {
    url: 'UserManage/GetUser',
    method: 'get',
    responseType: 'json',
    isFormData: false,
    async: async (token = '') =>
      await axios.get('UserManage/GetUser', {
        ...InitialConfig,
        headers: {
          Authorization: token,
          'Content-Type': 'application/json;charset=UTF-8'
        }
      })
  }
}

export default Endpoints
