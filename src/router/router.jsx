import Footer from 'components/Footer'
import Navbar from 'components/Navbar'
import Preloader from 'components/Preloader'
import Sidebar from 'components/Sidebar'
import { DevRoutes, Routes } from 'constants/routes'
import React, { lazy, Suspense, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect, Route, Switch } from 'react-router-dom'
import SimpleBar from 'simplebar-react'
import {
  fetchUserAsync,
  resetUser,
  selectToken,
  selectUser
} from 'store/slices/UserSlice'

//********************* DEVELOPMENT *********************/
// pages
import DashboardOverview from 'pages/_dev/dashboard/DashboardOverview'
import ForgotPassword from 'pages/_dev/examples/ForgotPassword'
import Lock from 'pages/_dev/examples/Lock'
import NotFoundPage from 'pages/_dev/examples/NotFound'
import ResetPassword from 'pages/_dev/examples/ResetPassword'
import ServerError from 'pages/_dev/examples/ServerError'
import Signin from 'pages/_dev/examples/Signin'
import Signup from 'pages/_dev/examples/Signup'
import Presentation from 'pages/_dev/Presentation'
import Settings from 'pages/_dev/Settings'
import BootstrapTables from 'pages/_dev/tables/BootstrapTables'
import Transactions from 'pages/_dev/Transactions'
import Upgrade from 'pages/_dev/Upgrade'

// documentation pages
import DocsBuild from 'pages/_dev/documentation/DocsBuild'
import DocsChangelog from 'pages/_dev/documentation/DocsChangelog'
import DocsDownload from 'pages/_dev/documentation/DocsDownload'
import DocsFolderStructure from 'pages/_dev/documentation/DocsFolderStructure'
import DocsLicense from 'pages/_dev/documentation/DocsLicense'
import DocsOverview from 'pages/_dev/documentation/DocsOverview'
import DocsQuickStart from 'pages/_dev/documentation/DocsQuickStart'

import Accordion from 'pages/_dev/containers/Accordion'
import Alerts from 'pages/_dev/containers/Alerts'
import Badges from 'pages/_dev/containers/Badges'
import Breadcrumbs from 'pages/_dev/containers/Breadcrumbs'
import Buttons from 'pages/_dev/containers/Buttons'
import Forms from 'pages/_dev/containers/Forms'
import Modals from 'pages/_dev/containers/Modals'
import Navbars from 'pages/_dev/containers/Navbars'
import Navs from 'pages/_dev/containers/Navs'
import Pagination from 'pages/_dev/containers/Pagination'
import Popovers from 'pages/_dev/containers/Popovers'
import Progress from 'pages/_dev/containers/Progress'
import Tables from 'pages/_dev/containers/Tables'
import Tabs from 'pages/_dev/containers/Tabs'
import Toasts from 'pages/_dev/containers/Toasts'
import Tooltips from 'pages/_dev/containers/Tooltips'

const RouteWithLoader = ({ component: Component, ...rest }) => {
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    const timer = setTimeout(() => setLoaded(true), 1000)
    return () => clearTimeout(timer)
  }, [])

  return (
    <Route
      {...rest}
      render={props => (
        <Preloader show={loaded ? false : true}>
          <Component {...props} />
        </Preloader>
      )}
    />
  )
}

const RouteWithSidebar = ({ component: Component, title, admin, ...rest }) => {
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    const timer = setTimeout(() => setLoaded(true), 1000)
    return () => clearTimeout(timer)
  }, [])

  return (
    <Route
      {...rest}
      render={props => (
        <Preloader show={loaded ? false : true}>
          <Sidebar isAdmin={admin} />

          <main className={'content'}>
            <Navbar title={title} />
            <SimpleBar className={'main-content'} autoHide={false}>
              <div className={'wrapper'}>
                <div className="d-flex flex-column align-items-stretch flex-grow-1">
                  <Component {...props} />
                </div>
                <Footer />
              </div>
            </SimpleBar>
          </main>
        </Preloader>
      )}
    />
  )
}

const RouteWithAuth = ({
  private: isPrivate,
  auth: isAuthenticated,
  page,
  container: Component,
  ...rest
}) => {
  const Page = lazy(() => import(`pages/${page}`))

  return (isAuthenticated && isPrivate) || (!isAuthenticated && !isPrivate) ? (
    <Component
      {...rest}
      component={props => (
        <Suspense fallback={<Preloader show={true} relative={true} />}>
          <Page {...props} isAdmin={rest.admin} />
        </Suspense>
      )}
    />
  ) : isPrivate ? (
    <Redirect to={process.env.REACT_APP_AUTH_FAIL || '/login/'} />
  ) : (
    <Redirect to={process.env.REACT_APP_AUTH_SUCCESS || '/'} />
  )
}

const Router = () => {
  const [isAuth, setIsAuth] = useState(true)
  const [isLoading, setIsLoading] = useState(true)
  const dispatch = useDispatch()
  const user = useSelector(selectUser)
  const token = useSelector(selectToken)

  useEffect(() => {
    // TODO: UPDATE HERE
    // setIsAuth(!!token)
    // !token ? dispatch(resetUser()) : dispatch(fetchUserAsync(token))
  }, [token, dispatch])

  useEffect(() => {
    setIsLoading(false)
    console.log('-------------HERE')
    console.log(user)
  }, [user])

  return isLoading ? (
    <Preloader show={true} />
  ) : (
    <Switch>
      {/* Public Routes */}
      <RouteWithAuth
        exact
        container={RouteWithLoader}
        {...Routes.Login}
        auth={false}
      />

      {/* Private Routes */}
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Dashboard}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Dataset}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Data}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Request}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Support}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Setting}
        auth={isAuth}
      />

      {/* Admin Routes */}
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Admin.Dashboard}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Admin.Dataset}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Admin.DatasetUser}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Admin.Process}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Admin.Request}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Admin.User}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Admin.Role}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Admin.Activity}
        auth={isAuth}
      />
      <RouteWithAuth
        exact
        container={RouteWithSidebar}
        {...Routes.Admin.Support}
        auth={isAuth}
      />

      {/* DEVELOPMENT */}
      <RouteWithLoader
        exact
        path={DevRoutes.Presentation.path}
        component={Presentation}
      />
      <RouteWithLoader exact path={DevRoutes.Signin.path} component={Signin} />
      <RouteWithLoader exact path={DevRoutes.Signup.path} component={Signup} />
      <RouteWithLoader
        exact
        path={DevRoutes.ForgotPassword.path}
        component={ForgotPassword}
      />
      <RouteWithLoader
        exact
        path={DevRoutes.ResetPassword.path}
        component={ResetPassword}
      />
      <RouteWithLoader exact path={DevRoutes.Lock.path} component={Lock} />
      <RouteWithLoader
        exact
        path={DevRoutes.NotFound.path}
        component={NotFoundPage}
      />
      <RouteWithLoader
        exact
        path={DevRoutes.ServerError.path}
        component={ServerError}
      />

      {/* pages */}
      <RouteWithSidebar
        exact
        path={DevRoutes.DashboardOverview.path}
        component={DashboardOverview}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.Upgrade.path}
        component={Upgrade}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.Transactions.path}
        component={Transactions}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.Settings.path}
        component={Settings}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.BootstrapTables.path}
        component={BootstrapTables}
      />

      {/* components */}
      <RouteWithSidebar
        exact
        path={DevRoutes.Accordions.path}
        component={Accordion}
      />
      <RouteWithSidebar exact path={DevRoutes.Alerts.path} component={Alerts} />
      <RouteWithSidebar exact path={DevRoutes.Badges.path} component={Badges} />
      <RouteWithSidebar
        exact
        path={DevRoutes.Breadcrumbs.path}
        component={Breadcrumbs}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.Buttons.path}
        component={Buttons}
      />
      <RouteWithSidebar exact path={DevRoutes.Forms.path} component={Forms} />
      <RouteWithSidebar exact path={DevRoutes.Modals.path} component={Modals} />
      <RouteWithSidebar exact path={DevRoutes.Navs.path} component={Navs} />
      <RouteWithSidebar
        exact
        path={DevRoutes.Navbars.path}
        component={Navbars}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.Pagination.path}
        component={Pagination}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.Popovers.path}
        component={Popovers}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.Progress.path}
        component={Progress}
      />
      <RouteWithSidebar exact path={DevRoutes.Tables.path} component={Tables} />
      <RouteWithSidebar exact path={DevRoutes.Tabs.path} component={Tabs} />
      <RouteWithSidebar
        exact
        path={DevRoutes.Tooltips.path}
        component={Tooltips}
      />
      <RouteWithSidebar exact path={DevRoutes.Toasts.path} component={Toasts} />

      {/* documentation */}
      <RouteWithSidebar
        exact
        path={DevRoutes.DocsOverview.path}
        component={DocsOverview}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.DocsDownload.path}
        component={DocsDownload}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.DocsQuickStart.path}
        component={DocsQuickStart}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.DocsLicense.path}
        component={DocsLicense}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.DocsFolderStructure.path}
        component={DocsFolderStructure}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.DocsBuild.path}
        component={DocsBuild}
      />
      <RouteWithSidebar
        exact
        path={DevRoutes.DocsChangelog.path}
        component={DocsChangelog}
      />

      <Redirect to={DevRoutes.NotFound.path} />
    </Switch>
  )
}

export default Router
