import { persistCombineReducers } from 'reduxjs-toolkit-persist'
import autoMergeLevel2 from 'reduxjs-toolkit-persist/lib/stateReconciler/autoMergeLevel2'
import storage from 'reduxjs-toolkit-persist/lib/storage'
import reducers from './reducers'

const persistConfig = {
  key: 'mcihub',
  version: 1,
  storage,
  // whitelist: undefined,
  // blacklist: undefined,
  // transforms: undefined,
  stateReconciler: autoMergeLevel2,
  debug: true
}

const persistedReducer = persistCombineReducers(persistConfig, reducers)

export default persistedReducer
