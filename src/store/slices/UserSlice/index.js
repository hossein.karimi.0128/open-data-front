import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import Endpoints from 'service/endpoints'

const initialState = {
  token: '',
  data: undefined
}
export const fetchUserAsync = createAsyncThunk(
  'user/fetchUser',
  async token => {
    const response = await Endpoints.fetchUser.async(token)
    return response.data
  }
)

export const UserSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setToken: (state, action) => {
      state.token = action.payload
    },
    setUser: (state, action) => {
      state.data = action.payload
    },
    resetUser: state => {
      state.token = ''
      state.data = undefined
    }
  },
  extraReducers: builder => {
    builder
      .addCase(fetchUserAsync.fulfilled, (state, action) => {
        state.data = action.payload
      })
      .addCase(fetchUserAsync.rejected, (state, action) => {
        console.log('ERRRRRRRROOOOOOOOOR')
      })
  }
})

export const selectToken = state => state.user.token
export const selectUser = state => state.user.data

export const { setToken, setUser, resetUser } = UserSlice.actions

export default UserSlice.reducer
