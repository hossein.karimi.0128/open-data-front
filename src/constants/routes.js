import {
  faChartLine,
  faChartPie,
  faCogs,
  faDatabase,
  faFileAlt,
  faHandsHelping,
  faList,
  faMagic,
  faUserFriends
} from '@fortawesome/free-solid-svg-icons'

export const DevRoutes = {
  // pages
  Presentation: { path: '/_dev/' },
  DashboardOverview: { path: '/_dev/dashboard/overview' },
  Transactions: { path: '/_dev/transactions' },
  Settings: { path: '/_dev/settings' },
  Upgrade: { path: '/_dev/upgrade' },
  BootstrapTables: { path: '/_dev/tables/bootstrap-tables' },
  Billing: { path: '/_dev/examples/billing' },
  Invoice: { path: '/_dev/examples/invoice' },
  Signin: { path: '/_dev/examples/sign-in' },
  Signup: { path: '/_dev/examples/sign-up' },
  ForgotPassword: { path: '/_dev/examples/forgot-password' },
  ResetPassword: { path: '/_dev/examples/reset-password' },
  Lock: { path: '/_dev/examples/lock' },
  NotFound: { path: '/_dev/examples/404' },
  ServerError: { path: '/_dev/examples/500' },

  // docs
  DocsOverview: { path: '/_dev/documentation/overview' },
  DocsDownload: { path: '/_dev/documentation/download' },
  DocsQuickStart: { path: '/_dev/documentation/quick-start' },
  DocsLicense: { path: '/_dev/documentation/license' },
  DocsFolderStructure: { path: '/_dev/documentation/folder-structure' },
  DocsBuild: { path: '/_dev/documentation/build-tools' },
  DocsChangelog: { path: '/_dev/documentation/changelog' },

  // components
  Accordions: { path: '/_dev/components/accordions' },
  Alerts: { path: '/_dev/components/alerts' },
  Badges: { path: '/_dev/components/badges' },
  Widgets: { path: '/_dev/widgets' },
  Breadcrumbs: { path: '/_dev/components/breadcrumbs' },
  Buttons: { path: '/_dev/components/buttons' },
  Forms: { path: '/_dev/components/forms' },
  Modals: { path: '/_dev/components/modals' },
  Navs: { path: '/_dev/components/navs' },
  Navbars: { path: '/_dev/components/navbars' },
  Pagination: { path: '/_dev/components/pagination' },
  Popovers: { path: '/_dev/components/popovers' },
  Progress: { path: '/_dev/components/progress' },
  Tables: { path: '/_dev/components/tables' },
  Tabs: { path: '/_dev/components/tabs' },
  Tooltips: { path: '/_dev/components/tooltips' },
  Toasts: { path: '/_dev/components/toasts' },
  WidgetsComponent: { path: '/_dev/components/widgets' }
}

export const Routes = {
  // Public Routes
  Login: {
    path: '/login/',
    page: 'Login',
    private: false,
    title: 'ورود به پنل',
    admin: false,
    sidebar: false,
    icon: null
  },

  // Private Routes
  // User Routes
  Dashboard: {
    path: '/',
    page: 'Dashboard',
    private: true,
    title: 'داشبورد',
    admin: false,
    sidebar: true,
    icon: faChartPie
  },
  Dataset: {
    path: '/dataset/',
    page: 'Dataset',
    private: true,
    title: 'دادگان',
    admin: false,
    sidebar: true,
    icon: faDatabase
  },
  Data: {
    path: '/data/',
    page: 'Data',
    private: true,
    title: 'داده‌های من',
    admin: false,
    sidebar: true,
    icon: faFileAlt
  },
  Request: {
    path: '/request/',
    page: 'Request',
    private: true,
    title: 'درخواست‌ها',
    admin: false,
    sidebar: true,
    icon: faList
  },
  Support: {
    path: '/support/',
    page: 'Support',
    private: true,
    title: 'پشتیبانی',
    admin: false,
    sidebar: true,
    icon: faHandsHelping
  },
  Setting: {
    path: '/setting/',
    page: 'Setting',
    private: true,
    title: 'تنظیمات',
    admin: false,
    sidebar: true,
    icon: faCogs
  },
  //Admin Routes
  Admin: {
    Dashboard: {
      path: '/admin/',
      page: 'Dashboard',
      private: true,
      title: 'داشبورد',
      admin: true,
      sidebar: true,
      icon: faChartPie
    },
    Dataset: {
      path: '/admin/dataset/',
      page: 'Dataset',
      private: true,
      title: 'مدیریت دادگان',
      admin: true,
      sidebar: true,
      icon: faDatabase
    },
    DatasetUser: {
      path: '/admin/dataset/user/',
      page: 'User',
      private: true,
      title: 'کاربران مجاز',
      admin: true,
      sidebar: false,
      icon: null
    },
    Process: {
      path: '/admin/dataset/process/',
      page: 'Process',
      private: true,
      title: 'عملیات دادگان',
      admin: true,
      sidebar: false,
      icon: null
    },
    Request: {
      path: '/admin/request/',
      page: 'Request',
      private: true,
      title: 'مدیریت درخواست‌ها',
      admin: true,
      sidebar: true,
      icon: faList
    },
    User: {
      path: '/admin/user/',
      page: 'User',
      private: true,
      title: 'مدیریت کاربران',
      admin: true,
      sidebar: true,
      icon: faUserFriends
    },
    Role: {
      path: '/admin/role/',
      page: 'Role',
      private: true,
      title: 'مدیریت نقش‌ها',
      admin: true,
      sidebar: true,
      icon: faMagic
    },
    Activity: {
      path: '/admin/activity/',
      page: 'Activity',
      private: true,
      title: 'مدیریت عملیات‌ها',
      admin: true,
      sidebar: true,
      icon: faChartLine
    },
    Support: {
      path: '/admin/support/',
      page: 'Support',
      private: true,
      title: 'پشتیبانی',
      admin: true,
      sidebar: false,
      icon: faHandsHelping
    }
  }
}
