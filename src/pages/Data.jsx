import { faDownload } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button, Form } from '@themesberg/react-bootstrap'
import Table from 'components/Table'
import React, { useState } from 'react'
import { Cell, Column, HeaderCell } from 'rsuite-table'
import 'rsuite-table/dist/css/rsuite-table.css'

const items = [
  {
    id: 1,
    name: 'مجموعه دادگان ۱',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 2,
    name: 'مجموعه دادگان ۲',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 3,
    name: 'مجموعه دادگان ۳',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 4,
    name: 'مجموعه دادگان ۴',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 5,
    name: 'مجموعه دادگان ۵',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 6,
    name: 'مجموعه دادگان ۶',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 7,
    name: 'مجموعه دادگان ۷',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 8,
    name: 'مجموعه دادگان ۸',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 9,
    name: 'مجموعه دادگان ۹',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 10,
    name: 'مجموعه دادگان ۱۰',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 11,
    name: 'مجموعه دادگان ۱۱',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 12,
    name: 'مجموعه دادگان ۱۲',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  }
]

const columns = [
  { key: 'id', value: 'ID' },
  { key: 'name', value: 'نام مجموعه دادگان' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' }
]

export default props => {
  const { isAdmin } = props
  const [selected, setSelected] = useState([])

  const onCheckHandle = event => {
    const { value, checked } = event.target
    checked
      ? setSelected([...selected, Number(value)])
      : setSelected(selected.filter(item => item !== Number(value)))
  }

  return (
    <Table
      items={items}
      // columns={columns}
      actions={
        <div className={'d-flex flex-row-reverse align-items-center gap-2'}>
          {selected.length > 0 && (
            <Button variant={'success'} onClick={() => alert('Download!')}>
              دانلود
            </Button>
          )}
        </div>
      }
    >
      <Column verticalAlign={'middle'} align={'center'} width={42}>
        <HeaderCell></HeaderCell>
        <Cell dataKey={'id'}>
          {rowData => (
            <Form.Check type={'checkbox'} id={`id-${rowData.id}`}>
              <Form.Check.Input
                type={'checkbox'}
                name={`row-${rowData.id}`}
                value={rowData.id}
                checked={!!selected.find(item => item === rowData.id)}
                onChange={onCheckHandle}
              />
              <Form.Check.Label
                className={
                  'position-absolute top-0 bottom-0 start-0 end-0 cursor-pointer'
                }
              />
            </Form.Check>
          )}
        </Cell>
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>نام مجموعه دادگان</HeaderCell>
        <Cell dataKey={'name'} />
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>نوع مجموعه دادگان</HeaderCell>
        <Cell dataKey={'type'} />
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>جنس داده</HeaderCell>
        <Cell dataKey={'type'} />
      </Column>

      <Column flexGrow={3} verticalAlign={'middle'}>
        <HeaderCell>شرح داده</HeaderCell>
        <Cell dataKey={'description'} className={'text-truncate'} />
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>تاریخ انتشار</HeaderCell>
        <Cell dataKey={'published'} />
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>تاریخ انقضا</HeaderCell>
        <Cell dataKey={'updated'} />
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>حجم فایل</HeaderCell>
        <Cell dataKey={'records'} />
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>تعداد رکورد</HeaderCell>
        <Cell dataKey={'records'} />
      </Column>

      {isAdmin && (
        <>
          <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
            <HeaderCell>وضعیت پردازش</HeaderCell>
            <Cell dataKey={'records'} />
          </Column>
          <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
            <HeaderCell>وضعیت گمنام‌سازی</HeaderCell>
            <Cell dataKey={'records'} />
          </Column>
          <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
            <HeaderCell>وضعیت انتشار</HeaderCell>
            <Cell dataKey={'records'} />
          </Column>
        </>
      )}

      <Column verticalAlign={'middle'} align={'center'} width={64}>
        <HeaderCell>فایل شرح دادگان</HeaderCell>
        <Cell dataKey={'records'}>
          <FontAwesomeIcon
            icon={faDownload}
            size={'lg'}
            onClick={() => alert('Download!')}
          />
        </Cell>
      </Column>
    </Table>
  )
}
