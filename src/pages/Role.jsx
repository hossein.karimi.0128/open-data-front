import {
  Button,
  Card,
  Col,
  Form,
  ListGroup,
  Row
} from '@themesberg/react-bootstrap'
import React, { useState } from 'react'

const roleItems = [
  { id: 1, title: 'ادمین سیستم' },
  { id: 2, title: 'مدیر مجوز' },
  { id: 3, title: 'اپراتور عملیات' },
  { id: 4, title: 'پشتیبان' },
  { id: 5, title: 'کاربر عادی' }
]

const permissionItems = [
  { id: 1, title: 'درخواست دادگان' },
  { id: 2, title: 'مشاهده لیست درخواست' },
  { id: 3, title: 'مدیریت درخواست' },
  { id: 4, title: 'اجرای عملیات' },
  { id: 5, title: 'ویرایش عملیات' },
  { id: 6, title: 'بارگذاری دادگان' },
  { id: 7, title: 'مشاهده کاربران' },
  { id: 8, title: 'مدیریت کاربران' },
  { id: 9, title: 'مشاهده نقش‌ها' },
  { id: 10, title: 'مدیریت نقش‌ها' },
  { id: 11, title: 'تخصیص نقش ادمین' }
]

export default () => {
  const [roles, setRoles] = useState(roleItems)
  const [newRole, setNewRole] = useState({ id: 2, title: '' })

  const onChangeHandle = event => {
    const title = event.target.value
    const id = roles[roles.length - 1].id + 1
    setNewRole({ id, title })
  }

  return (
    <div
      className={
        'd-flex flex-column align-items-center justify-content-start gap-3 mt-5 mb-3'
      }
    >
      <div
        className={
          'd-flex align-items-center justify-content-start gap-3 w-100'
        }
      >
        <Row className={'w-50 align-items-end gap-2 mx-0'}>
          <Form.Group as={Col} controlId={'role'} className={'px-0'}>
            <Form.Label>نام نقش</Form.Label>
            <Form.Control
              type={'text'}
              placeholder={'نام نقش را وارد کنید'}
              onChange={onChangeHandle}
              value={newRole.title}
            />
          </Form.Group>

          <Form.Group as={Col} controlId={'role'} className={'px-0'}>
            <Button
              variant={'success'}
              onClick={() => setRoles([...roles, newRole])}
              disabled={newRole.title.length === 0}
            >
              افزودن نقش جدید
            </Button>
          </Form.Group>
        </Row>
      </div>

      {roles.map((role, rindex) => (
        <Card key={`role-${rindex}`} className={'w-100'} bg={'light'}>
          <Card.Header className={'fw-bold py-3'}>{role.title}</Card.Header>
          <Card.Body className={'p-2'}>
            <ListGroup horizontal className={'flex-wrap'}>
              {permissionItems.map((item, index) => (
                <ListGroup.Item
                  key={`permission-${rindex}-${index}`}
                  as={'li'}
                  className={
                    'd-flex justify-content-start align-items-center gap-2 px-2 py-1 flex-fill w-25 bg-transparent'
                  }
                >
                  <Form.Check
                    type={'checkbox'}
                    id={`permission-${rindex}-${index}`}
                  />
                  <Form.Label
                    htmlFor={`permission-${rindex}-${index}`}
                    className={'fs-6 fw-normal m-0'}
                  >
                    {item.title}
                  </Form.Label>
                </ListGroup.Item>
              ))}
              <li className="d-flex justify-content-start align-items-center gap-2 px-2 py-1 flex-fill w-25 bg-transparent" />
            </ListGroup>
          </Card.Body>
        </Card>
      ))}
    </div>
  )
}
