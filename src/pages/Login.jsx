import {
  Button,
  Card,
  Container,
  Form,
  Image,
  Spinner
} from '@themesberg/react-bootstrap'
import Logo from 'assets/img/logo-light.png'
import useHttp from 'hooks/useHttp'
import React, { useEffect, useState } from 'react'
import { toEnglish, toPersian } from 'src/helper/formatter'

export default () => {
  const [step, setStep] = useState(1)
  const [data, setData] = useState({ mobileNumber: '', code: '' })
  const [maxLength, setMaxLength] = useState(11)
  const [isLoadingM, responseM, errorM, onRequestM] = useHttp()
  const [isLoadingC, responseC, errorC, onRequestC] = useHttp()

  useEffect(() => {
    setMaxLength(data.mobileNumber.startsWith('0') ? 11 : 10)
    return () => {
      setMaxLength(11)
    }
  }, [data.mobileNumber])

  useEffect(() => {
    if (responseM?.isSuccess) {
      setStep(2)
    }
    if (errorM) {
      console.log('----- Error')
    }
  }, [responseM, errorM])

  useEffect(() => {
    if (responseC?.isSuccess) {
      console.log('SET TOKEN -----------------')
    }
    if (errorC) {
      console.log('----- Error')
    }
  }, [responseC, errorC])

  const onHandleChange = event => {
    const { name, value } = event.target
    setData({ ...data, [name]: toEnglish(value).replace(/\D/, '') })
  }

  const onSubmitMobile = event => {
    event.preventDefault()
    if (step === 1) {
      onRequestM('verifyMobile', { mobileNumber: data.mobileNumber })
    } else {
      onRequestC('verifyOtp', {
        mobileNumber: data.mobileNumber,
        code: data.code
      })
    }
  }

  return (
    <div
      className={
        'd-flex align-items-center justify-content-center min-vh-100 p-5 bg-soft-primary'
      }
    >
      <Container fluid>
        <Card bg={'white'} className={'p-3 shadow w-100 fmxw-400 mx-auto'}>
          <Card.Body
            as={'form'}
            autoComplete={'off'}
            className={'d-flex flex-column align-items-stretch gap-3'}
            onSubmit={onSubmitMobile}
          >
            <Image src={Logo} className={'mx-auto mb-4'} />

            {step === 1 && (
              <>
                <Form.Group
                  className={'form-floating'}
                  controlId={'mobileNumber'}
                >
                  <Form.Control
                    name={'mobileNumber'}
                    type={'tel'}
                    size={'lg'}
                    placeholder={'شماره موبایل را وارد کنید'}
                    autoComplete={'off'}
                    className={'text-black fw-medium text-ltr text-center ls-3'}
                    maxLength={maxLength}
                    onChange={onHandleChange}
                    value={toPersian(data.mobileNumber)}
                  />
                  <Form.Label className={'m-0'}>شماره موبایل</Form.Label>
                </Form.Group>

                <Button
                  variant={'success'}
                  size={'lg'}
                  type={'submit'}
                  className={'fw-medium fs-7'}
                  disabled={
                    data.mobileNumber.length !== maxLength || isLoadingM
                  }
                >
                  {isLoadingM ? (
                    <Spinner animation={'border'} size={'sm'} />
                  ) : (
                    'ارسال پیامک'
                  )}
                </Button>
              </>
            )}

            {step === 2 && (
              <>
                <Form.Group className={'form-floating'} controlId={'code'}>
                  <Form.Control
                    name={'code'}
                    type={'tel'}
                    size={'lg'}
                    placeholder={'کد ارسال شده به موبایل را وارد کنید'}
                    autoComplete={'off'}
                    className={'text-black fw-medium text-ltr text-center ls-3'}
                    maxLength={6}
                    onChange={onHandleChange}
                    value={toPersian(data.code)}
                  />
                  <Form.Label className={'m-0'}>
                    کد ارسال شده به موبایل
                  </Form.Label>
                </Form.Group>

                <Button
                  variant={'success'}
                  size={'lg'}
                  type={'submit'}
                  className={'fw-medium fs-7'}
                  disabled={data.code.length !== 6 || isLoadingC}
                >
                  {isLoadingC ? (
                    <Spinner animation={'border'} size={'sm'} />
                  ) : (
                    'ورود به پنل'
                  )}
                </Button>
              </>
            )}
          </Card.Body>
        </Card>
      </Container>
    </div>
  )
}
