import { faDownload } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button, Form, Modal } from '@themesberg/react-bootstrap'
import Table from 'components/Table'
import { Routes } from 'constants/routes'
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Cell, Column, HeaderCell } from 'rsuite-table'
import 'rsuite-table/dist/css/rsuite-table.css'

const items = [
  {
    id: 1,
    name: 'مجموعه دادگان ۱',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 2,
    name: 'مجموعه دادگان ۲',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 3,
    name: 'مجموعه دادگان ۳',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 4,
    name: 'مجموعه دادگان ۴',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 5,
    name: 'مجموعه دادگان ۵',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 6,
    name: 'مجموعه دادگان ۶',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 7,
    name: 'مجموعه دادگان ۷',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 8,
    name: 'مجموعه دادگان ۸',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 9,
    name: 'مجموعه دادگان ۹',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 10,
    name: 'مجموعه دادگان ۱۰',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 11,
    name: 'مجموعه دادگان ۱۱',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  },
  {
    id: 12,
    name: 'مجموعه دادگان ۱۲',
    type: 'نوع ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200
  }
]

const columns = [
  { key: 'id', value: 'ID' },
  { key: 'name', value: 'نام مجموعه دادگان' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' }
]

export default props => {
  const { push } = useHistory()
  const { isAdmin } = props
  const [selected, setSelected] = useState([])
  const [isModal, setIsModal] = useState(false)

  const onCheckHandle = event => {
    const { value, checked } = event.target
    checked
      ? setSelected([Number(value)])
      : setSelected(selected.filter(item => item !== Number(value)))
  }

  const onCloseModal = () => setIsModal(false)
  const onShowModal = type => setIsModal(type)

  const onGenerateActionButtons = {
    true: (
      <>
        <Button
          variant="primary"
          onClick={() => push(Routes.Admin.Process.path)}
        >
          عملیات
        </Button>
        <Button
          variant="primary"
          onClick={() => push(Routes.Admin.DatasetUser.path)}
        >
          کاربران مجاز
        </Button>
        <Button variant="primary" onClick={() => alert('آزادسازی انجام شد!')}>
          آزادسازی
        </Button>
        <Button variant="primary" onClick={() => alert('انتشار انجام شد!')}>
          انتشار
        </Button>
        <Button variant="danger" onClick={() => onShowModal('delete')}>
          حذف
        </Button>
      </>
    ),
    false: (
      <>
        <Button variant="success" onClick={() => onShowModal('request')}>
          درخواست
        </Button>
      </>
    )
  }

  const onGenerateTitle = {
    request: 'درخواست دسترسی',
    add: 'افزودن دادگان جدید',
    delete: 'حذف مجموعه دادگان؟'
  }

  const onGenerateBody = {
    request: (
      <Form>
        <Form.Group className={'mb-3'}>
          <Form.Label>هدف از درخواست</Form.Label>
          <Form.Select className={'mb-1'} defaultValue={''}>
            <option disabled value={''}>
              انتخاب کنید
            </option>
            <option value={'1'}>استفاده تجاری</option>
            <option value={'2'}>استفاده تحقیقاتی</option>
            <option value={'3'}>مسابقه</option>
            <option value={'4'}>درون سازمانی</option>
          </Form.Select>
          <Form.Text className={'text-muted'}>
            * با انتخاب صحیح دسته‌بندی درخواست مجموعه دادگان ما را در بهبود
            سامانه یاری کنید.
          </Form.Text>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>شرح درخواست</Form.Label>
          <Form.Control as={'textarea'} rows={4} />
        </Form.Group>
        <Form.Group>
          <Form.Label>بارگذاری سند شرح دادگان</Form.Label>
          <Form.Control type={'file'} />
        </Form.Group>
      </Form>
    ),
    add: (
      <Form>
        <Form.Group className={'mb-3'}>
          <Form.Label>نام مجموعه داده</Form.Label>
          <Form.Control />
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>مجوز انتشار</Form.Label>
          <Form.Select className={'mb-1'} defaultValue={''}>
            <option disabled value={''}>
              انتخاب کنید
            </option>
            <option value={'1'}>لایسنس اول</option>
            <option value={'2'}>لایسنس دوم</option>
            <option value={'3'}>لایسنس سوم</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>کاربرد</Form.Label>
          <Form.Select className={'mb-1'} defaultValue={''}>
            <option disabled value={''}>
              انتخاب کنید
            </option>
            <option value={'1'}>پیش‌بینی ریزش مشتری</option>
            <option value={'2'}>تحلیل رفتار مشترکین</option>
            <option value={'3'}>اعتبار مشترکین</option>
            <option value={'4'}>تشخصی ناهنجاری در مصرف</option>
            <option value={'5'}>تشخیص ناهنجاری در بار مضاعف زیرساخت</option>
            <option value={'6'}>درک زبان طبیعی</option>
            <option value={'7'}>پردازش زبان طبیعی</option>
            <option value={'8'}>چت بات فارسی</option>
            <option value={'9'}>پیش‌بینی مصرف و پیشنهاد بسته</option>
            <option value={'10'}>خوشه‌بندی مشتریان</option>
            <option value={'11'}>تحلیل و عیب‌یابی خطاها</option>
            <option value={'12'}>تحلیل سلامت شبکه</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>شرح مجموعه</Form.Label>
          <Form.Control as={'textarea'} rows={4} />
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>بارگذاری فایل csv مجموعه دادگان</Form.Label>
          <Form.Control type={'file'} accept={'.csv'} />
        </Form.Group>
        <Form.Group>
          <Form.Label>بارگذاری فایل سند شرح دادگان</Form.Label>
          <Form.Control type={'file'} />
        </Form.Group>
      </Form>
    ),
    delete: (
      <div>
        {`شما در حال حذف ${items
          .filter(item => selected.includes(item.id))
          .map(item => item.name)
          .join(' و ')} هستید.`}
        <br />
        با حذف این مجموعه‌ی دادگان علاوه بر حذف این مجموعه از پنل ادمین از دسترس
        تمامی کاربران درخواست داده نیز خارج خواهد شد.
      </div>
    )
  }

  const onGenerateFooter = {
    request: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button variant={'success'} onClick={onCloseModal}>
          ثبت درخواست
        </Button>
      </>
    ),
    add: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button variant={'success'} onClick={onCloseModal}>
          افزودن دادگان
        </Button>
      </>
    ),
    delete: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button variant={'success'} onClick={onCloseModal}>
          حذف دادگان
        </Button>
      </>
    )
  }

  return (
    <Table
      items={items}
      // columns={columns}
      actions={
        <div className={'d-flex flex-row-reverse align-items-center gap-2'}>
          {isAdmin && (
            <Button variant="success" onClick={() => onShowModal('add')}>
              افزودن دادگان
            </Button>
          )}
          {selected.length > 0 && onGenerateActionButtons[isAdmin]}
          <Modal show={!!isModal} onHide={onCloseModal}>
            <Modal.Header closeButton>
              <Modal.Title>{onGenerateTitle[isModal]}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{onGenerateBody[isModal]}</Modal.Body>
            <Modal.Footer>{onGenerateFooter[isModal]}</Modal.Footer>
          </Modal>
        </div>
      }
    >
      <Column verticalAlign={'middle'} align={'center'} width={42}>
        <HeaderCell></HeaderCell>
        <Cell dataKey={'id'}>
          {rowData => (
            <Form.Check type={'checkbox'} id={`id-${rowData.id}`}>
              <Form.Check.Input
                type={'checkbox'}
                name={`row-${rowData.id}`}
                value={rowData.id}
                checked={!!selected.find(item => item === rowData.id)}
                onChange={onCheckHandle}
              />
              <Form.Check.Label
                className={
                  'position-absolute top-0 bottom-0 start-0 end-0 cursor-pointer'
                }
              />
            </Form.Check>
          )}
        </Cell>
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>نام مجموعه دادگان</HeaderCell>
        <Cell dataKey={'name'} />
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>نوع مجموعه دادگان</HeaderCell>
        <Cell dataKey={'type'} />
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>جنس داده</HeaderCell>
        <Cell dataKey={'type'} />
      </Column>

      <Column flexGrow={3} verticalAlign={'middle'}>
        <HeaderCell>شرح داده</HeaderCell>
        <Cell dataKey={'description'} className={'text-truncate'} />
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>تاریخ انتشار</HeaderCell>
        <Cell dataKey={'published'} />
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>تاریخ انقضا</HeaderCell>
        <Cell dataKey={'updated'} />
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>حجم فایل</HeaderCell>
        <Cell dataKey={'records'} />
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>تعداد رکورد</HeaderCell>
        <Cell dataKey={'records'} />
      </Column>

      {isAdmin && (
        <>
          <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
            <HeaderCell>وضعیت پردازش</HeaderCell>
            <Cell dataKey={'records'} />
          </Column>
          <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
            <HeaderCell>وضعیت گمنام‌سازی</HeaderCell>
            <Cell dataKey={'records'} />
          </Column>
          <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
            <HeaderCell>وضعیت انتشار</HeaderCell>
            <Cell dataKey={'records'} />
          </Column>
        </>
      )}

      <Column verticalAlign={'middle'} align={'center'} width={64}>
        <HeaderCell>فایل شرح دادگان</HeaderCell>
        <Cell dataKey={'records'}>
          <FontAwesomeIcon
            icon={faDownload}
            size={'lg'}
            onClick={() => alert('Download!')}
          />
        </Cell>
      </Column>
    </Table>
  )
}
