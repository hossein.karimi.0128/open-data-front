import { Button, Form, Modal } from '@themesberg/react-bootstrap'
import Table from 'components/Table'
import { Routes } from 'constants/routes'
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Cell, Column, HeaderCell } from 'rsuite-table'
import 'rsuite-table/dist/css/rsuite-table.css'

const items = [
  {
    id: 1,
    name: 'علی اصغر مدیرروستا',
    username: 'asghar',
    phone: '۰۹۱۲۳۴۵۶۷۸۹',
    role: 'کاربر عادی',
    registered: '۱۴۰۱/۰۳/۰۳',
    updated: '۱۴۰۱/۰۵/۰۵',
    type: 'کاربر سازمانی'
  },
  {
    id: 2,
    name: 'علی اصغر مدیرروستا',
    username: 'asghar',
    phone: '۰۹۱۲۳۴۵۶۷۸۹',
    role: 'کاربر عادی',
    registered: '۱۴۰۱/۰۳/۰۳',
    updated: '۱۴۰۱/۰۵/۰۵',
    type: 'کاربر سازمانی'
  },
  {
    id: 3,
    name: 'علی اصغر مدیرروستا',
    username: 'asghar',
    phone: '۰۹۱۲۳۴۵۶۷۸۹',
    role: 'کاربر عادی',
    registered: '۱۴۰۱/۰۳/۰۳',
    updated: '۱۴۰۱/۰۵/۰۵',
    type: 'کاربر سازمانی'
  },
  {
    id: 4,
    name: 'علی اصغر مدیرروستا',
    username: 'asghar',
    phone: '۰۹۱۲۳۴۵۶۷۸۹',
    role: 'کاربر عادی',
    registered: '۱۴۰۱/۰۳/۰۳',
    updated: '۱۴۰۱/۰۵/۰۵',
    type: 'کاربر سازمانی'
  }
]

const columns = [
  { key: 'id', value: 'ID' },
  { key: 'name', value: 'نام مجموعه دادگان' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' }
]

export default props => {
  const { push } = useHistory()
  const [selected, setSelected] = useState([])
  const [isModal, setIsModal] = useState(false)

  const onCheckHandle = event => {
    const { value, checked } = event.target
    checked
      ? setSelected([Number(value)])
      : setSelected(selected.filter(item => item !== Number(value)))
  }

  const onCloseModal = () => setIsModal(false)
  const onShowModal = type => setIsModal(type)

  const onGenerateActionButtons = (
    <>
      <Button variant="primary" onClick={() => push(Routes.Admin.Request.path)}>
        درخواست‌ها
      </Button>

      <Button variant="primary" onClick={() => alert('دسترسی‌ها!')}>
        دسترسی‌ها
      </Button>
      <Button variant="primary" onClick={() => alert('ویرایش!')}>
        ویرایش
      </Button>
      <Button variant="danger" onClick={() => onShowModal('delete')}>
        حذف
      </Button>
    </>
  )

  const onGenerateTitle = {
    add: 'افزودن کاربر جدید',
    delete: 'حذف کاربر'
  }

  const onGenerateBody = {
    add: (
      <Form>
        <Form.Group className={'mb-3'}>
          <Form.Label>نام کاربری</Form.Label>
          <Form.Control type={'text'} />
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>نام و نام خانوادگی</Form.Label>
          <Form.Control type={'text'} />
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>شماره تماس</Form.Label>
          <Form.Control type={'text'} />
        </Form.Group>
        <Form.Group>
          <Form.Label>نقش کاربر</Form.Label>
          <Form.Select className={'mb-1'} defaultValue={''}>
            <option disabled value={''}>
              انتخاب کنید
            </option>
            <option value={'1'}>کاربر عادی</option>
            <option value={'2'}>پشتیبان</option>
            <option value={'3'}>اپراتور عملیات</option>
            <option value={'4'}>مدیر مجوز</option>
            <option value={'5'}>ادمین سیستم</option>
          </Form.Select>
        </Form.Group>
      </Form>
    ),
    delete: (
      <div>
        {`شما در حال حذف ${items
          .filter(item => selected.includes(item.id))
          .map(item => item.name)
          .join(' و ')} هستید.`}
      </div>
    )
  }

  const onGenerateFooter = {
    add: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button variant={'success'} onClick={onCloseModal}>
          افزودن کاربر
        </Button>
      </>
    ),
    delete: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button variant={'danger'} onClick={onCloseModal}>
          حذف کاربر
        </Button>
      </>
    )
  }

  return (
    <Table
      items={items}
      // columns={columns}
      actions={
        <div className={'d-flex flex-row-reverse align-items-center gap-2'}>
          <Button variant="success" onClick={() => onShowModal('add')}>
            افزودن کاربر
          </Button>
          {selected.length > 0 && onGenerateActionButtons}
          <Modal show={!!isModal} onHide={onCloseModal}>
            <Modal.Header closeButton>
              <Modal.Title>{onGenerateTitle[isModal]}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{onGenerateBody[isModal]}</Modal.Body>
            <Modal.Footer>{onGenerateFooter[isModal]}</Modal.Footer>
          </Modal>
        </div>
      }
    >
      <Column verticalAlign={'middle'} align={'center'} width={42}>
        <HeaderCell></HeaderCell>
        <Cell dataKey={'id'}>
          {rowData => (
            <Form.Check type={'checkbox'} id={`id-${rowData.id}`}>
              <Form.Check.Input
                type={'checkbox'}
                name={`row-${rowData.id}`}
                value={rowData.id}
                checked={!!selected.find(item => item === rowData.id)}
                onChange={onCheckHandle}
              />
              <Form.Check.Label
                className={
                  'position-absolute top-0 bottom-0 start-0 end-0 cursor-pointer'
                }
              />
            </Form.Check>
          )}
        </Cell>
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>نام و نام خانوادگی</HeaderCell>
        <Cell dataKey={'name'} />
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>نام کاربری</HeaderCell>
        <Cell dataKey={'username'} />
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>شماره تماس</HeaderCell>
        <Cell dataKey={'phone'} />
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>نقش کاربر</HeaderCell>
        <Cell dataKey={'role'} />
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>تاریخ عضویت</HeaderCell>
        <Cell dataKey={'registered'} />
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>آخرین فعالیت</HeaderCell>
        <Cell dataKey={'updated'} />
      </Column>
      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>نوع کاربر</HeaderCell>
        <Cell dataKey={'type'} />
      </Column>
    </Table>
  )
}
