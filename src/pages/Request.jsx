import { faDownload, faLink } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button, Form, Modal } from '@themesberg/react-bootstrap'
import Table from 'components/Table'
import React, { useState } from 'react'
import { Cell, Column, HeaderCell } from 'rsuite-table'
import 'rsuite-table/dist/css/rsuite-table.css'

const items = [
  {
    id: 1,
    name: 'مجموعه دادگان ۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  },
  {
    id: 2,
    name: 'مجموعه دادگان ۲',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  },
  {
    id: 3,
    name: 'مجموعه دادگان ۳',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  },
  {
    id: 4,
    name: 'مجموعه دادگان ۴',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  },
  {
    id: 5,
    name: 'مجموعه دادگان ۵',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  },
  {
    id: 6,
    name: 'مجموعه دادگان ۶',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  },
  {
    id: 7,
    name: 'مجموعه دادگان ۷',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  },
  {
    id: 8,
    name: 'مجموعه دادگان ۸',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  },
  {
    id: 9,
    name: 'مجموعه دادگان ۹',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  },
  {
    id: 10,
    name: 'مجموعه دادگان ۱۰',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  },
  {
    id: 11,
    name: 'مجموعه دادگان ۱۱',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  },
  {
    id: 12,
    name: 'مجموعه دادگان ۱۲',
    description:
      'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
    updated: '۱۴۰۱/۰۵/۰۱',
    published: '۱۴۰۱/۰۵/۰۱',
    records: 200,
    status: 'در حال بررسی'
  }
]

const columns = [
  { key: 'id', value: 'ID' },
  { key: 'name', value: 'نام مجموعه دادگان' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' },
  { key: '', value: '' }
]

export default props => {
  const { isAdmin } = props
  const [selected, setSelected] = useState([])
  const [isModal, setIsModal] = useState(false)

  const onCheckHandle = event => {
    const { value, checked } = event.target
    checked
      ? setSelected(isAdmin ? [...selected, Number(value)] : [Number(value)])
      : setSelected(selected.filter(item => item !== Number(value)))
  }

  const onCloseModal = () => setIsModal(false)
  const onShowModal = type => setIsModal(type)

  const onGenerateActionButtons = {
    true: (
      <>
        <Button variant="success" onClick={() => onShowModal('confirm')}>
          تأیید
        </Button>
        <Button variant="danger" onClick={() => onShowModal('reject')}>
          رد
        </Button>
      </>
    ),
    false: (
      <>
        <Button variant="success" onClick={() => alert('ویرایش درخواست')}>
          ویرایش
        </Button>
      </>
    )
  }

  const onGenerateTitle = {
    check: 'بررسی دسترسی',
    confirm: 'تأیید تعلق دسترسی',
    reject: 'رد تعلق دسترسی'
  }

  const onGenerateBody = {
    check: (
      <Form>
        <Form.Group className={'mb-3'}>
          <Form.Label>هدف از درخواست</Form.Label>
          <Form.Select className={'mb-1'} defaultValue={''} disabled>
            <option disabled value={'2'}>
              انتخاب کنید
            </option>
            <option value={'1'}>استفاده تجاری</option>
            <option value={'2'}>استفاده تحقیقاتی</option>
            <option value={'3'}>مسابقه</option>
            <option value={'4'}>درون سازمانی</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Button onClick={() => alert('Download!')}>
            <FontAwesomeIcon icon={faDownload} pull={'right'} size={'lg'} />
            <span>دانلود اسناد بارگذاری شده</span>
          </Button>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>پاسخ به درخواست</Form.Label>
          <Form.Control as={'textarea'} rows={4} />
        </Form.Group>
        <Form.Group>
          <Form.Check type={'checkbox'} id={'confirm-1'}>
            <Form.Check.Input type={'checkbox'} />
            <Form.Check.Label className={'cursor-pointer ms-2'}>
              تأیید دسترسی پس از ارسال
            </Form.Check.Label>
          </Form.Check>

          <Form.Check type={'checkbox'} id={'reject-1'}>
            <Form.Check.Input type={'checkbox'} />
            <Form.Check.Label className={'cursor-pointer ms-2'}>
              رد دسترسی پس از ارسال
            </Form.Check.Label>
          </Form.Check>
        </Form.Group>
      </Form>
    ),
    confirm: (
      <div>
        {`شما در حال تأیید تعلق دسترسی ${items
          .filter(item => selected.includes(item.id))
          .map(item => item.name)
          .join(' و ')} هستید.`}
        <br />
        با حذف این مجموعه‌ی دادگان علاوه بر حذف این مجموعه از پنل ادمین از دسترس
        تمامی کاربران درخواست داده نیز خارج خواهد شد.
      </div>
    ),
    reject: (
      <div>
        {`شما در حال رد تعلق دسترسی ${items
          .filter(item => selected.includes(item.id))
          .map(item => item.name)
          .join(' و ')} هستید.`}
        <br />
        با حذف این مجموعه‌ی دادگان علاوه بر حذف این مجموعه از پنل ادمین از دسترس
        تمامی کاربران درخواست داده نیز خارج خواهد شد.
      </div>
    )
  }

  const onGenerateFooter = {
    check: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button variant={'success'} onClick={onCloseModal}>
          ثبت درخواست
        </Button>
      </>
    ),
    confirm: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button variant={'success'} onClick={onCloseModal}>
          تعلق درخواست
        </Button>
      </>
    ),
    reject: (
      <>
        <Button variant={'danger'} onClick={onCloseModal}>
          بستن
        </Button>
        <Button variant={'success'} onClick={onCloseModal}>
          رد درخواست
        </Button>
      </>
    )
  }

  return (
    <Table
      items={items}
      // columns={columns}
      actions={
        <div className={'d-flex flex-row-reverse align-items-center gap-2'}>
          {selected.length > 0 && onGenerateActionButtons[isAdmin]}
          <Modal show={!!isModal} onHide={onCloseModal}>
            <Modal.Header closeButton>
              <Modal.Title>{onGenerateTitle[isModal]}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{onGenerateBody[isModal]}</Modal.Body>
            <Modal.Footer>{onGenerateFooter[isModal]}</Modal.Footer>
          </Modal>
        </div>
      }
    >
      <Column verticalAlign={'middle'} align={'center'} width={42}>
        <HeaderCell></HeaderCell>
        <Cell dataKey={'id'}>
          {rowData => (
            <Form.Check type={'checkbox'} id={`id-${rowData.id}`}>
              <Form.Check.Input
                type={'checkbox'}
                name={`row-${rowData.id}`}
                value={rowData.id}
                checked={!!selected.find(item => item === rowData.id)}
                onChange={onCheckHandle}
              />
              <Form.Check.Label
                className={
                  'position-absolute top-0 bottom-0 start-0 end-0 cursor-pointer'
                }
              />
            </Form.Check>
          )}
        </Cell>
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'}>
        <HeaderCell>نام مجموعه دادگان</HeaderCell>
        <Cell dataKey={'name'}>
          {rowData => (
            <span
              role={'button'}
              className={'d-flex align-items-center'}
              onClick={() => onShowModal('check')}
            >
              <FontAwesomeIcon icon={faLink} pull={'right'} size={'sm'} />
              {rowData.name}
            </span>
          )}
        </Cell>
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'}>
        <HeaderCell>مرحله درخواست</HeaderCell>
        <Cell dataKey={'status'} />
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>تاریخ درخواست</HeaderCell>
        <Cell dataKey={'published'} />
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>آخرین به‌روزرسانی</HeaderCell>
        <Cell dataKey={'updated'} />
      </Column>

      <Column flexGrow={1} verticalAlign={'middle'} align={'center'}>
        <HeaderCell>نتیجه درخواست</HeaderCell>
        <Cell dataKey={'status'} />
      </Column>
    </Table>
  )
}
