import { Form } from '@themesberg/react-bootstrap'
import React, { useState } from 'react'
import 'rsuite-table/dist/css/rsuite-table.css'
import 'scss/dataset.scss'

const processList = [
  {
    title: 'استانداردسازی',
    children: [
      {
        title: 'سطح دادگان',
        children: [{ title: 'نام فیلد' }, { title: 'واحد' }]
      },
      {
        title: 'سطح پارامتر',
        children: [
          { title: 'صحت فیلد' },
          { title: 'مقدار فیلد' },
          { title: 'مقیاس' }
        ]
      },
      { title: 'کدگذاری داده‌ها', children: [{ title: 'encoding' }] }
    ]
  },
  {
    title: 'پاک‌سازی',
    children: [
      {
        title: 'عملیات پاک‌سازی داده پایه‌ای',
        children: [
          { title: 'شناسایی و حذف ستون‌های تکراری' },
          { title: 'شناسایی و حذف ستون‌های تک‌مقداری' },
          { title: 'شناسایی و حذف ردیف‌های تکراری' },
          { title: 'حذف ستون‌هایی با تعداد بسیار اندک' }
        ]
      },
      {
        title: 'شناسایی و حذف داده‌های پرت',
        children: [
          { title: 'روش انحراف معیار' },
          { title: 'روش محدوده بین چارکی' },
          { title: 'روش خودکار (مبتنی بر فاصله)' }
        ]
      },
      {
        title: 'شناسایی و حذف داده‌های از دست رفته',
        children: [
          { title: 'علامت‌گذاری مقادیر از دست رفته' },
          { title: 'حذف ردیف‌هایی با مقادیر از دست رفته' }
        ]
      },
      {
        title: 'جایگزین کردن مقادیر از دست رفته',
        children: [
          { title: 'استفاده از شاخص‌های آماری' },
          { title: 'استفاده از روش KNN' },
          { title: 'استفاده از روش تکرارشونده' }
        ]
      }
    ]
  },
  {
    title: 'نرمال‌سازی',
    children: [
      {
        title: 'Rescaling',
        children: [{ title: '' }]
      },
      {
        title: 'Z-Score',
        children: [{ title: '' }]
      },
      {
        title: 'Mean Normalization',
        children: [{ title: '' }]
      },
      {
        title: 'Feature Clipping',
        children: [{ title: '' }]
      },
      {
        title: 'Log Scaling',
        children: [{ title: '' }]
      }
    ]
  },
  {
    title: 'گمنام‌سازی',
    children: [
      {
        title: 'بی‌نام‌سازی قابل بازگشت',
        children: [{ title: 'رمزنگاری' }]
      },
      {
        title: 'بی‌نام‌سازی به غیرقابل بازگشت',
        children: [{ title: 'ماسک کردن' }, { title: 'هش کردن' }]
      }
    ]
  }
]

export default () => {
  const [selected, setSelected] = useState([])

  const onCheckHandle = event => {
    const { value, checked } = event.target
    checked
      ? setSelected([...selected, Number(value)])
      : setSelected(selected.filter(item => item !== Number(value)))
  }

  const onGenerateProcess = () => {
    const levels = { level1: [], level2: [], level3: [] }

    processList.forEach((level1, l1) => {
      levels.level1.push({
        title: level1.title,
        count: level1?.children?.length || 1,
        cols: 0
      })
      level1.children.forEach((level2, l2) => {
        levels.level2.push({
          title: level2.title,
          count: level2?.children?.length || 1,
          cols: level2?.children?.length || 1
        })
        level2.children.forEach((level3, l3) => {
          levels.level1[l1]['cols']++
          levels.level3.push({
            title: level3.title,
            count: level3?.children?.length || 1,
            cols: level3?.children?.length || 1
          })
        })
      })
    })

    return (
      <table className={'process'}>
        <thead>
          <tr className={'title level-1'}>
            <td rowSpan={3}>نام فیلد</td>
            {levels.level1.map((item, index) => (
              <td key={`level1-${index}`} colSpan={item.cols}>
                {item.title}
              </td>
            ))}
          </tr>
          <tr className={'title level-2'}>
            {levels.level2.map((item, index) => (
              <td key={`level2-${index}`} colSpan={item.cols}>
                {item.title}
              </td>
            ))}
          </tr>
          <tr className={'title level-3'}>
            {levels.level3.map((item, index) => (
              <td key={`level3-${index}`} colSpan={1}>
                {item.title}
              </td>
            ))}
          </tr>
        </thead>
        <tbody>
          {levels.level3.map((_item, row) => (
            <tr key={`item-${row}`}>
              <td>نام فیلد اینجا نوشته می‌شود</td>
              {levels.level3.map((_item, col) => (
                <td
                  key={`item-${col}`}
                  colSpan={1}
                  className={`${
                    (col * row) % 7 === 0
                      ? 'disabled'
                      : (col * row) % 8 === 0
                      ? 'checked'
                      : ''
                  }`}
                >
                  <Form.Check
                    type={'checkbox'}
                    name={`row-${col}`}
                    id={`id-${col}`}
                    onChange={onCheckHandle}
                    disabled={(col * row) % 7 === 0 && (col * row) % 8 > 0}
                    defaultChecked={
                      (col * row) % 8 === 0 && (col * row) % 7 > 0
                    }
                  />
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    )
  }

  return (
    <div
      className={'d-flex align-items-center justify-content-between mt-5 mb-3'}
    >
      {onGenerateProcess()}
    </div>
  )
}
