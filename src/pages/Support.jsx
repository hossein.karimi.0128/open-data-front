import { Form, Button } from '@themesberg/react-bootstrap'
import React from 'react'

export default () => {
  return (
    <div className={'p-5'}>
      <Form>
        <Form.Group className={'mb-3'}>
          <Form.Label>مجموعه دادگان</Form.Label>
          <Form.Select className={'mb-1'}>
            <option disabled selected>
              انتخاب کنید
            </option>
            <option value="1">مجموعه دادگان ۱</option>
            <option value="2">مجموعه دادگان ۲</option>
            <option value="3">مجموعه دادگان ۳</option>
          </Form.Select>
          <Form.Text className={'text-muted'}>
            * در صورتی که درخواست شما در مورد مجموعه دادگان خاصی است آن را
            انتخاب کنید.
          </Form.Text>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>دسته‌بندی درخواست</Form.Label>
          <Form.Select className={'mb-1'}>
            <option disabled selected>
              انتخاب کنید
            </option>
            <option value="1">پرسش یا ابهام</option>
            <option value="2">مشکل در دادگان</option>
            <option value="3">مشکل در سیستم</option>
            <option value="3">سایر</option>
          </Form.Select>
          <Form.Text className={'text-muted'}>
            * با انتخاب صحیح دسته‌بندی درخواست پشتیبانی ما را در بهبود سامانه
            یاری کنید.
          </Form.Text>
        </Form.Group>
        <Form.Group className={'mb-3'}>
          <Form.Label>شرح درخواست</Form.Label>
          <Form.Control as={'textarea'} rows={4} />
        </Form.Group>
        <Form.Group>
          <Button variant={'success'}>ثبت درخواست</Button>
        </Form.Group>
      </Form>
    </div>
  )
}
