import axios from 'axios'
import { useCallback, useState } from 'react'
import { useSelector } from 'react-redux'
import { InitialConfig, InitialError, PayloadType } from 'service/defaults'
import Endpoints from 'service/endpoints'

import { selectToken } from 'store/slices/UserSlice'

const http = axios.create(InitialConfig)
let responseInterceptor = null
let requestInterceptor = null

const useHttp = (isDeep = true) => {
  const [loading, setLoading] = useState(false)
  const [response, setResponse] = useState(undefined)
  const [error, setError] = useState(undefined)
  const token = useSelector(selectToken, () => true)
  if (token) http.defaults.headers.common['Authorization'] = `${token}`

  const onRejectInterceptor = useCallback((type, interceptor) => {
    if (!!interceptor || interceptor === 0) {
      http.interceptors[type].eject(interceptor)
    }
  }, [])

  onRejectInterceptor('request', requestInterceptor)
  onRejectInterceptor('response', responseInterceptor)

  requestInterceptor = http.interceptors.request.use(
    httpConfig => {
      return httpConfig
    },
    httpError => {
      return Promise.reject(httpError)
    }
  )

  responseInterceptor = http.interceptors.response.use(
    httpResponse => {
      return httpResponse
    },
    httpError => {
      if (httpError && httpError.response) {
        if (httpError.response.data.message) {
          return Promise.reject(httpError)
        }
      } else {
      }
      return Promise.reject(InitialError)
    }
  )

  const onRequest = useCallback(
    async (endpoint, payload) => {
      setLoading(true)
      const method = Endpoints[endpoint].method
      const url = Endpoints[endpoint].url
      const responseType = Endpoints[endpoint].responseType
      const isFormData = Endpoints[endpoint].isFormData
      const payloadData = isFormData ? new FormData() : payload

      if (isFormData) {
        for (const key in payload) {
          payloadData.append(key, payload[key])
        }
      }

      http.defaults.headers.common['Content-Type'] =
        PayloadType[isFormData ? 'formData' : 'default']
      http.defaults.responseType = responseType

      const data =
        method.toLowerCase() === 'get' || method.toLowerCase() === 'delete'
          ? { params: payloadData }
          : { data: payloadData }

      let resultData = undefined
      try {
        const result = await http.request({
          method,
          url,
          ...data
        })
        resultData = isDeep ? await result.data : result
        setResponse(resultData)
      } catch (error) {
        resultData = isDeep ? await error.response.data : error.response
        setError(resultData)
      } finally {
        setLoading(false)
      }
      return await resultData
    },
    [isDeep]
  )

  return [loading, response, error, onRequest]
}

export default useHttp
