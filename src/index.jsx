import Preloader from 'components/Preloader'
import ScrollToTop from 'components/ScrollToTop'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { persistStore } from 'reduxjs-toolkit-persist'
import { PersistGate } from 'reduxjs-toolkit-persist/integration/react'
import Routes from 'router/router'
import store from 'store/config/store'

// core styles
import 'bootstrap/dist/css/bootstrap.rtl.min.css'
import 'scss/volt.scss'

// vendor styles
import 'react-datetime/css/react-datetime.css'

const App = () => {
  const persistor = persistStore(store)

  return (
    <Provider store={store}>
      <PersistGate loading={<Preloader show={true} />} persistor={persistor}>
        <BrowserRouter>
          <ScrollToTop />
          <Routes />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))
